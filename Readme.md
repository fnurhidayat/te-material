# Flipped Learning Materials

![BinarXGlints](https://ik.imagekit.io/latihan/Glints___EX_3kWO.png)

Hello guys, this is a repository for materials that you will learn in the class.
Please read this material before the class. Because what we will do is flipped learning.
That means you have to read this before you attend the class.
Don't worry, if you still don't understand any materials in this repository,
You can ask me directly in my Telegram or Platform Group.

> *Education is the most powerful weapon which you can use to change the world.*

### Materials

This is the link to navigate you to specified material. 

* [01. Introduction](./Material/01.%20Introduction.md)
* [02. Git Introduction](./Material/02.%20Git%20Introduction.md)
* [03. Object Oriented Programming](./Material/03.%20Object%20Oriented%20Programming.md)

### Task

* Basic class
  * Week-01
    * [Daily Task #3](./01.%20Basic%20Class/01.%20Week-01/Task/03.%2022-04-2020.md)
  * Week-02
    * [Daily Task #6](./01.%20Basic%20Class/02.%20Week-02/Task/01.%2027-04-2020.md)
    * [Daily Task #7](./01.%20Basic%20Class/02.%20Week-02/Task/02.%2028-04-2020.md)
    * [Daily Task #8](./01.%20Basic%20Class/02.%20Week-02/Task/03.%2029-04-2020.md)
  * Week-03
    * [Daily Task #10 (Morning Class)](./01.%20Basic%20Class/03.%20Week-03/Task/01m.%2004-05-2020.md)
    * [Daily Task #11 (Morning Class)](./01.%20Basic%20Class/03.%20Week-03/Task/02m.%2005-05-2020.md)
    * [Daily Task #11 (Day Class)](./01.%20Basic%20Class/03.%20Week-03/Task/02d.%2005-05-2020.md)
    * [Daily Task #12](./01.%20Basic%20Class/03.%20Week-03/Task/03.%2006-05-2020.md)

### Code Challenge

* Basic class
  * [Week 01](./01.%20Basic%20Class/01.%20Week-01/Code%20Challenge/CC01.md)
  * [Week 02](./01.%20Basic%20Class/02.%20Week-02/Code%20Challenge/CC02.md)
